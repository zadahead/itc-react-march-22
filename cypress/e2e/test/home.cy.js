/// <reference types="cypress" />

/* eslint-disable no-undef */

describe('our first test', () => {
    it('will go main page', () => {
        cy.visit('http://localhost:3000/');

        cy.get('[data-testid="view-home"] .Input input').eq(0).type('mosh@gmail.com');
        cy.get('[data-testid="view-home"] .Input input').eq(1).type('mypass');

        cy.get('[data-testid="view-home"] [data-testid="btn"]').click();

        cy.location().should((loc) => {
            expect(loc.pathname).to.eq('/dropdown');
        })

        cy.get('[data-testid="view-dropdown"]').within(() => {
            cy.get('.Dropdown').click();

            cy.get('.Dropdown .list h4').eq(0).click();
            cy.get('.Dropdown .header h3').contains('Mosh');
        })
    })

    it('will go to Tweets page', () => {
        cy.visit('http://localhost:3000/tweets');

        cy.intercept(
            'GET',
            'https://micro-blogging-dot-full-stack-course-services.ew.r.appspot.com/tweet',
            { fixture: 'tweets.json' }
        ).as('GET_TWEETS')

        cy.wait('@GET_TWEETS');

        cy.get('[data-testid="view-tweets"]').within(() => {
            cy.get('h4').eq(0).contains('My Content!!');
        })
    })
})