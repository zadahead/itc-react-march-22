import { useEffect, useState } from "react";
import { Btn, Line, Rows } from "UIKit";

export const LifeCycleUnmount = () => {
    console.log('render-start');
    const [count, setCount] = useState(0);
    const [count2, setCount2] = useState(0);

    //onstart, onend
    useEffect(() => {
        console.log('onstart');

        const cleanUp = () => {
            console.log('onend');
        }

        return cleanUp;
    }, [])

    //onstart, onend, onupdate, onbeforeupdate
    useEffect(() => {
        console.log('onstart + onupdate', count);

        const cleanUp = () => {
            console.log('onend + onbeforeupdate', count);
        }

        return cleanUp;
    })

    //onstart [count], onend[count], onupdate[count], onbeforeupdate[count]
    useEffect(() => {
        console.log('onstart[count] + onupdate[count]', count);

        const cleanUp = () => {
            console.log('onend[count] + onbeforeupdate[count]', count);
        }

        return cleanUp;
    }, [count]);


    console.log('render');
    return (
        <div>
            <Rows>
            <h2>Hello Mosh</h2>
            <h3>count1: {count} -- count2: {count2}</h3>
            <Line>
                <Btn onClick={() => setCount(count + 1)}>Add 1</Btn>
                <Btn onClick={() => setCount2(count2 + 1)}>Add 2</Btn>
            </Line>
            </Rows>

        </div>
    )
}