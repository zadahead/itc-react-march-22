import { useState } from "react"
import { Line } from "../UIKit/Layouts/Line/Line";
import { Rows } from "../UIKit/Layouts/Rows/Rows";

export const Counter = (props) => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    return (
        <Rows>
            <h4>Counter</h4>
            <Line>
                <h4>Count</h4>
                {count}
                <button onClick={handleAdd}>Add</button>
            </Line>
        </Rows>
    )
}