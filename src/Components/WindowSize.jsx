import { useWindowSize } from "Hooks/useWindowSize";
import { Rows } from "UIKit"

export const WindowSize = () => {
    //logic
    const sizes = useWindowSize();

    
    //render
    return (
        <div>
            <Rows>
                <h3>Width: {sizes.width}</h3>
                <h3>Height: {sizes.height}</h3>
            </Rows>
        </div>
    )
}