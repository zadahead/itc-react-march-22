import { Line } from "../UIKit/Layouts/Line/Line"
import { Rows } from "../UIKit/Layouts/Rows/Rows"

/*
1) current value (state)
2) set value (setState)
3) initial value 
4) re-render 
*/


export const PropsCounter = (props) => {
    console.log('render');

    return (
        <Rows>
            <h1>Counter</h1>
            <Line>
                <h3>Count</h3>
                {props.count}
                <button onClick={props.handleAdd}>Add</button>
            </Line>
        </Rows>
    )
}
