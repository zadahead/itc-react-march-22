import { useColorSwitcher } from "Hooks/useColorSwitch";
import { Btn, Rows } from "UIKit"


export const ColorSwitcher = () => {
    //logic
    const { color, handleSwitch } = useColorSwitcher();


    //render
    const boxStyle = {
        width: '100px',
        height: '100px',
        backgroundColor: color
    }

    return (
        <div>
            <Rows>
                <div style={boxStyle}></div>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Rows>
        </div>
    )
}