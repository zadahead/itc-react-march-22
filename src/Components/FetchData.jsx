import { Fetch } from "UIKit/Widget/Fetch/Fetch";



export const FetchData = () => {

    return (
        <div>
            <h2>Mosh Todos</h2>
            <Fetch
                method="post"
                args={{ id: 1 }}
                path="/todos"
            />
        </div>
    )

}

export const FetchUserData = () => {

    return (
        <div>
            <h2>Mosh Users</h2>
            <Fetch
                path="/users"
                onRender={(users) => (
                    users.map(i => (
                        <h4 key={i.id}>{`${i.id}) ${i.name}`}</h4>
                    ))
                )}
            />
        </div>
    )

}