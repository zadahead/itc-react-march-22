import { useState } from "react"
import { Btn, Input, Line, Rows } from "UIKit";

/*
1) current value (state)
2) set value (setState)
3) initial value 
4) re-render 

[1, 2, 3, 4]

controlled component 
uncontrolled component 
*/
export const TextCard = () => {
    const [value, setValue] = useState('');

    const handleClear = () => {
        setValue('bla bla');
    }
    const handleLog = () => {
        console.log('value', value);
    }

    return (
        <Rows>
            <h3>Text Card</h3>
            <h5>{value}</h5>
            <Line>
                <Input value={value} onChange={setValue} onlynumber={true} />

                <Btn i="heart" theme="ok" onClick={handleLog}>Log This</Btn>
                <Btn i="user" theme="cancel" onClick={handleClear}>Clear</Btn>
                <Btn i="trash" theme="delete" onClick={handleClear}>Delete</Btn>
            </Line>
        </Rows>
    )
}