import { useState } from "react"


export const CubeGen = () => {

    const [list, setList] = useState([]);

    console.log(list);


    const handleGenerate = () => {
        var randomColor = Math.floor(Math.random() * 16777215).toString(16);

        setList([
            ...list,
            {
                id: Date.now(),
                x: rnd(window.innerWidth - 100),
                y: rnd(window.innerHeight - 100),
                backgroundColor: '#' + randomColor,
                size: rnd(100)
            }
        ])
    }

    const rnd = (max) => {
        return Math.floor(Math.random() * max);
    }

    const handleClick = (id) => {
        setList(list.filter(i => id !== i.id));
    }

    const renderList = () => {
        return list.map(i => {
            const cubeStyle = {
                top: `${i.y}px`,
                left: `${i.x}px`,
                position: 'absolute',
                backgroundColor: i.backgroundColor,
                width: `${i.size}px`,
                height: `${i.size}px`
            }

            return <div key={i.id} style={cubeStyle} onClick={() => { handleClick(i.id) }}></div>
        })
    }

    return (
        <div>
            <button onClick={handleGenerate}>Generate Cube</button>
            {renderList()}
        </div>
    )
}