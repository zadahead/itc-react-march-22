import { useColorSwitcher } from "Hooks/useColorSwitch";
import { useCounterRecap } from "Hooks/useCounterRecap"
import { Btn, Line, Rows } from "UIKit"

export const CombinedComp = () => {
    const { count, handleAdd} = useCounterRecap();
    const { color, handleSwitch } = useColorSwitcher();

    const styleCss = {
        color
    }

    return (
        <div>
            <Rows>
                <h3 style={styleCss}>Count, {count}</h3>
                <Line>
                    <Btn onClick={handleSwitch}>Switch</Btn>
                    <Btn onClick={handleAdd}>Add +</Btn>
                </Line>
            </Rows>
        </div>
    )
}