import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit"




export const CouterHook = () => {
    //logic
    const { count, handleAdd } = useCounter();

    //render
    return (
        <div>
            <Rows>
                <h2>CouterHook, {count}</h2>
                <Btn onClick={handleAdd}>Add</Btn>
            </Rows>
        </div>
    )
}

export const AnotherCount = () => {
    const { count, handleAdd } = useCounter();

    const renderTitle = () => {
        if (count > 5) {
            return <h2>HIGH</h2>
        }
        return <h2>Low</h2>
    }

    return (
        <div>
            <Rows>
                {renderTitle()}
                <Btn onClick={handleAdd}>Set</Btn>
            </Rows>
        </div>
    )
}