import { useState } from "react"
import { PropsCounter } from "./PropsCounter"

export const CounterWrapper = () => {
    let [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1); //after state has changed
    }

    const renderSecondCounter = () => {
        if (count % 2 === 0) {
            return <PropsCounter count={count} handleAdd={handleAdd} />
        }
        return null;
    }

    return (
        <div>
            <h3>Counter Wrapper</h3>

            <PropsCounter count={count} handleAdd={handleAdd} />
            {renderSecondCounter()}
        </div>
    )
}