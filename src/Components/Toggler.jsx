import { useState } from "react"
import { Rows } from "../UIKit/Layouts/Rows/Rows";

export const Toggler = (props) => {
    const [isShow, setIsShow] = useState(true);

    const handleToggle = () => {
        setIsShow(!isShow);
    }

    return (
        <Rows>
            <h3>Toggler</h3>
            <div>
                <button onClick={handleToggle}>Toggle</button>
            </div>
            {isShow && props.elem}
        </Rows>
    )
}