import { useCounterRecap } from "Hooks/useCounterRecap";
import { Btn } from "UIKit"

/*
    1) Create Component without any custom hooks
    2) Spot the logic and render
    3) create our custom hook, and paste the logic inside
    4) recognise the value that the render needs, and return them from custom hook
    5) use custom hook, and thats it
    6) take the custom and put it on a different location (/Hooks )
*/


export const CounterRecap = () => {
    //logic
    const { count, handleAdd } = useCounterRecap();

    //render
    return (
        <div>
            <h3>Count, {count}</h3>
            <Btn onClick={handleAdd}>Add +</Btn>
        </div>
    )
}