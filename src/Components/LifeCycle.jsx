
/*
    1) starts
    2) state updated
    3) removed
*/

import { useState } from "react";
import { useEffect } from "react";
import { Btn } from "UIKit";
import { Rows } from "../UIKit";

export const LifeCycle = () => {
    const [count, setCount] = useState(0);
    const [time, setTime] = useState("");

    const handleAdd = () => {
        setCount(count + 1);
    }

    useEffect(() => {
        console.log('timer....');
        setTimeout(() => {
            setTime(new Date().toLocaleTimeString())
        }, 1000);
    }, [time])

    return (
        <div>
            <Rows>
                <h3>{time}</h3>
                <h4>Count: {count}</h4>
                <div>
                    <Btn onClick={handleAdd}>Add</Btn>
                </div>
            </Rows>
        </div>
    )
}



/*
    const handleAdd = () => {
        console.log('handleAdd');
        setCount(0 + 1);
    }

    const handleAdd = () => {
        console.log('handleAdd');
        setCount(1 + 1);
    }



*/