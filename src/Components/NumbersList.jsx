
const list = [1, 2, 3, 4, 5];

const mappedList = list.map((i) => {
    return { id: i, value: i }
})

export const NumbersList = () => {



    const renderList = () => {
        return mappedList.map((i) => {
            return <h3 key={i.id}>{i.value}</h3>
        })
    }

    const getArrayItems = () => {
        return [
            <h1 key={11}>aaaa</h1>,
            <h2 key={21}>bbbb</h2>,
            <h3 key={'asadas'}>mosh</h3>
        ]
    }

    const combine = () => {
        const arr1 = renderList();
        const arr2 = getArrayItems();

        const combined = arr1.concat(arr2);

        return combined;
    }

    return (
        <div>
            <h1>Numbers List</h1>
            {renderList()}
        </div>
    )
}