import { useState } from "react";
import { Modal } from "../Modal/Modal";
import { Rows } from "../UIKit/Layouts/Rows/Rows"

export const Alerter = () => {
    const [isAlert, setIsAlert] = useState(false);

    const handleAlert = () => {
        setIsAlert(!isAlert);
    }

    const closeAlert = () => {
        setIsAlert(false);
    }

    return (
        <Rows>
            <h2>Aleter</h2>
            <div>
                <button onClick={handleAlert}>alert</button>
                {isAlert && <Modal text="Well done" onClose={closeAlert} />}
            </div>
        </Rows>
    )
}