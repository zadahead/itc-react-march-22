import { useEffect, useRef, useState } from "react";
import { Btn, Input, Line, Rows } from "UIKit";


export const Timer = () => {
    console.log('render');
    const [value, setValue] = useState('Hello Mosh');
    const [count, setCount] = useState(0);

    const countRef = useRef(0);
    const inputRef = useRef();

    useEffect(() => {
        inputRef.current.setAttribute('style', 'background-color:red')
    }, [])

    const handleAdd = () => {
        setCount(count + 1)
    }

    const handleAddRef = () => {
        countRef.current++;

        console.log(countRef.current);
    }

    return (
        <div>
            <Rows>
                <h2>Timer, {count}</h2>
                <div>
                    <Input ref={inputRef} id="myInput" value={value} onChange={setValue} />
                </div>
                <div>
                    <Line>
                        <Btn onClick={handleAddRef}>Add Ref</Btn>
                        <Btn onClick={handleAdd}>Add</Btn>
                    </Line>
                </div>
            </Rows>
        </div>
    )
}