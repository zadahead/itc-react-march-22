
import axios from 'axios';
import { useEffect } from 'react';
import { useState } from 'react';

import localForage from "localforage";




export const Tweets = () => {
    const [list, setList] = useState(null);

    useEffect(() => {
        fetchData();
    }, []);

    const getLocalTweets = async () => {
        try {
            const value = await localForage.getItem('tweets');
            if (!value) {
                fetchData();
            } else if (value) {
                setList(value);
            }
        } catch (error) {

        }
    }

    const fetchData = () => {
        axios.get('https://micro-blogging-dot-full-stack-course-services.ew.r.appspot.com/tweet')
            .then(resp => {
                const data = resp.data.tweets;
                console.log(data);
                setList(data);

                handleLocalForage(data);
            })
    }

    const handleLocalForage = async (data) => {
        await localForage.setItem('tweets', data);
    }

    const renderList = () => {
        return list.map(i => {
            return <h4 key={i.id}>{i.content}</h4>
        })
    }

    return (
        <div>
            <h2>Mosh Tweets</h2>
            {list ? renderList() : <h5>loading....</h5>}
        </div>
    )
}

/*

state => list => setList

useEffect => []

list.map 


*/