import { useState } from "react";

export const useColorSwitcher = () => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const color = isRed ? 'red' : 'blue';

    return {
        handleSwitch,
        color
    }
}