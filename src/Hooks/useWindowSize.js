import { useState, useEffect } from "react";

export const useWindowSize = () => {
    const [sizes, setSizes] = useState({});

    useEffect(() => {
        window.addEventListener('resize', handleResize);
        handleResize();

        return () => {
            window.removeEventListener('resize', handleResize);
        }
    }, []);
   

    const handleResize = () => {
        setSizes({
            width: window.innerWidth,
            height: window.innerHeight
        })
    }

    return sizes;
}