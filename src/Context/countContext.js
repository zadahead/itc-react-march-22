import { createContext, useState } from "react";

export const counterContext = createContext();

const Provider = counterContext.Provider;
 
export const CounterProvider = (props) => {

    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }

    const value = {
        count, 
        handleAdd
    }

    return (
        <Provider value={value}>
            {props.children}
        </Provider>
    )
}

