import { createContext } from "react";

export const moshContext = createContext();

const Provider = moshContext.Provider;

export const MoshProvider = ({ children }) => {
    return (
        <Provider value="Hello Mosh111">
            {children}
        </Provider>
    )
}