import { useContext } from "react"
import { counterContext } from "Context/countContext"
import { moshContext } from "Context/moshContext";

export const AboutView = () => {
    const { count } = useContext(counterContext);
    const msg = useContext(moshContext);

    return (
        <div>
            <h3>About us, {count}</h3>
            <h4>{msg}</h4>
        </div>
    )
}