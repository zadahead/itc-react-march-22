import { LifeCycle } from "../Components/LifeCycle"
import { Btn, Rows } from "UIKit";
import { LifeCycleUnmount } from "Components/LifeCycleSummary";
import { useState } from "react";


export const LifeCycleView = () => {
    const [count, setCount] = useState(0);

    const handleAdd = () => {
        setCount(count + 1);
    }
    return (
        <div>
            <Rows>
                <h4>Life Cycle, {count}</h4>
                <div>
                    <Btn onClick={handleAdd}>Add</Btn>
                </div>
                {/* <LifeCycle /> */}
                <LifeCycleUnmount />
            </Rows>
        </div>
    )
}