import { useState } from "react"
import { useNavigate } from "react-router";
import { Btn, Input } from "UIKit"

export const HomeView = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('/dropdown');
    }

    const isDisabled = !username || !password;

    return (
        <div data-testid="view-home">
            <h3>Welcome Home</h3>

            <Input value={username} onChange={setUsername} placeholder="Username" />
            <Input value={password} onChange={setPassword} placeholder="Password" />

            <Btn onClick={handleClick} disabled={isDisabled}>Login</Btn>

        </div>
    )
}
