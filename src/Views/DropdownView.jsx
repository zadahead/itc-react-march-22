import { useState } from "react"
import { Dropdown, Rows } from "UIKit"

const list = [
    { id: 1, name: 'Mosh' },
    { id: 2, name: 'David' },
    { id: 3, name: 'Bob' },
    { id: 4, name: 'Habanai' }
]

export const DropdownView = () => {
    const [selected, setSelected] = useState(null);

    return (
        <div data-testid="view-dropdown">
            <Rows>
                <h3>Dropdown View</h3>
                <Dropdown list={list} selected={selected} onChange={setSelected} />
                <h5>under</h5>
            </Rows>
        </div>
    )
}