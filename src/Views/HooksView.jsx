import { ColorSwitcher } from "Components/ColorSwitcher"
import { CombinedComp } from "Components/CombinedComp"
import { CounterRecap } from "Components/CounterRecap"
import { AnotherCount, CouterHook } from "Components/CouterHook"
import { FetchData, FetchUserData } from "Components/FetchData"
import { WindowSize } from "Components/WindowSize"
import { Rows } from "UIKit"

export const HooksView = () => {
    return (
        <div>
            <Rows>
                <FetchUserData />
                <FetchData />
                {/* <WindowSize />
                <CombinedComp />
                <ColorSwitcher />
                <CounterRecap />
                <CouterHook />
                <AnotherCount /> */}
            </Rows>
        </div>
    )
}