import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { HomeView } from "Views/HomeView";
import { type } from "TestLib/helper";

/*
    Test HomeView
        verify button is disabled on start
        verify this is disable if one of the fields is empty
        veiry this is enabled, if none is empty
*/
describe('<HomeView />', () => {
    it('will render 2 inputs and a button', () => {
        const { btn, username, password } = init();

        expect(username).toBeInTheDocument();
        expect(password).toBeInTheDocument();

        expect(btn).toBeInTheDocument();
    })

    it('will test if the button is disable when inputs are empty', () => {
        const { btn, username, password } = init();

        //make sure bothe fields are empty
        expect(username.value).toBe('');
        expect(password.value).toBe('');

        expect(btn).toBeDisabled();

        //make sure is it disabled when password empty
        type(username, 'hi ');
        type(password, '');

        expect(btn).toBeDisabled();

        //make sure is it disabled when username empty
        type(username, '');
        type(password, 'dd');

        expect(btn).toBeDisabled();

        //make sure this is enabled when none is empty
        type(username, 'hi ');
        type(password, 'dd ');

        expect(btn).toBeEnabled();
    })
})

const init = () => {
    render(<HomeView />);

    const btn = screen.getByRole('button');
    const inputs = screen.getAllByRole('textbox');
    const [username, password] = inputs;

    return {
        btn,
        username,
        password
    }
}
