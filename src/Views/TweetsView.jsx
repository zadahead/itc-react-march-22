import { Tweets } from "Components/Tweets"

export const TweetsView = () => {
    return (
        <div data-testid="view-tweets">
            <Tweets></Tweets>
        </div>
    )
}