import './Grid.css';

export const Grid = (props) => {
    return (
        <div className='Grid'>
            <header>{props.header}</header>
            <section>{props.section}</section>
            <footer>{props.footer}</footer>
        </div>
    )
}