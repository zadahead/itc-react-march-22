import { useEffect } from 'react';
import { useState } from 'react';
import { Between, Icon } from 'UIKit';
import './Dropdown.css';

export const Dropdown = ({ list = [], selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', handleClose);

        return () => {
            document.body.removeEventListener('click', handleClose);
        }
    }, [])

    const handleToggle = (e) => {
        e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const handleSelect = (itemId) => {
        onChange(itemId);
        setIsOpen(false);
    }

    const handleClose = () => {
        setIsOpen(false);
    }

    const renderTitle = () => {
        if (selected) {
            const item = list.find(i => i.id === selected);
            if (item) {
                return item.name;
            }
        }
        return 'Please Select'
    }

    const renderList = () => (
        list.map(i => (
            <h4
                key={i.id}
                className={`${i.id === selected ? 'selected' : ''}`}
                onClick={() => handleSelect(i.id)}
            >
                {i.name}
            </h4>
        ))
    )

    return (
        <div className="Dropdown">
            <div className='header' onClick={handleToggle}>
                <Between>
                    <h3>{renderTitle()}</h3>
                    <Icon icon="chevron-down" />
                </Between>
            </div>
            {isOpen && (
                <div className='list'>
                    {renderList()}
                </div>
            )}
        </div>
    )
}