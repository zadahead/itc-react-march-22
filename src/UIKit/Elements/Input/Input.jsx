import { forwardRef } from 'react';
import './Input.css';

const Input = forwardRef((props, ref) => {
    const { onlynumber, ...rest } = props;

    const handleChange = (e) => {
        const value = e.target.value;
        const canChange = onlynumber ? !isNaN(value) : true;
        if (canChange) {
            props.onChange(e.target.value);
        }
    }

    return (
        <div className="Input">
            <input
                {...rest}
                onChange={handleChange}
                ref={ref}
            />
        </div>
    )
})

export {
    Input
};