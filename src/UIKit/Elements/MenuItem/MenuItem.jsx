import { Line } from "../../Layouts/Line/Line"
import { Icon } from "../Icon/Icon"

export const MenuItem = (props) => {
    return (
        <Line>
            <Icon icon={props.icon} />
            <h5>{props.title}</h5>
        </Line>
    )
}