import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Btn } from "./Btn";


describe('<Btn />', () => {
    it('will render the Btn', () => {
        //Assign
        render(<Btn />);

        //Act
        const btn = screen.getByRole('button');

        //Assert
        expect(btn).toBeInTheDocument();
    })

    it('will render a Btn with label', () => {
        render(<Btn>Click Me</Btn>);

        const label = screen.getByTestId('btn-label');
        expect(label).toBeInTheDocument();

        const value = label.innerHTML;
        expect(value).toEqual('Click Me');
    })

    it('will render a Btn with an icon', () => {
        render(<Btn i="heart">Click Me</Btn>);

        const icons = screen.getAllByTestId('icon');

        expect(icons.length).toBe(1);

        const [icon] = icons;

        expect(icon).toBeInTheDocument();

        expect(icon.classList.contains('fa-heart')).toBe(true);
    })

    it('will trigger a function on click', () => {

        const func = jest.fn();

        render(<Btn onClick={func}>Click Me</Btn>);

        const btn = screen.getByTestId('btn');

        fireEvent.click(btn);

        expect(func).toBeCalledTimes(1);
    })
})