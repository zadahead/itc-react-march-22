import { Between } from 'UIKit/Layouts/Line/Line';
import { Icon } from '../Icon/Icon';

import './Btn.css';

export const Btn = (props) => {

    return (
        <button
            data-testid="btn"
            onClick={props.onClick}
            theme={props.theme}
            className={`Btn ${props.i && 'with-icon'}`}
            disabled={props.disabled}
        >
            <Between>
                {props.i && <Icon icon={props.i} />}
                <div data-testid="btn-label">
                    {props.children}
                </div>
            </Between>
        </button>
    )
}