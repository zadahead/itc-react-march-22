export const Icon = (props) => {
    return (
        <i data-testid="icon" className={`far fa-${props.icon}`} onClick={props.onClick}></i>
    )
}