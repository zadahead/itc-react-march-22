import ReactDOM from 'react-dom/client';

import { App } from './App';

import './index.css';

//react router dom
import { BrowserRouter } from "react-router-dom";
import { CounterProvider } from 'Context/countContext';
import { MoshProvider } from 'Context/moshContext';

//root, settings page
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <CounterProvider>
      <MoshProvider>
        <App />
      </MoshProvider>
    </CounterProvider>
  </BrowserRouter>
);




