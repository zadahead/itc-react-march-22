import { fireEvent } from "@testing-library/dom";

export const type = (elem, value) => {
    fireEvent.change(elem, { target: { value } });
}