import React, { useContext } from 'react';

import { Line, Between, Rows, Grid, Icon, Btn } from 'UIKit';
import { CubeGen } from './Components/CubeGen';

import { Routes, Route, Link, NavLink, Navigate } from 'react-router-dom';
import { HomeView } from './Views/HomeView';
import { AboutView } from './Views/AboutView';

import './App.css';
import { LifeCycleView } from './Views/LifeCycleView';
import { TweetsView } from 'Views/TweetsView';
import { RefsView } from 'Views/RefsView';
import { HooksView } from 'Views/HooksView';
import { DropdownView } from 'Views/DropdownView';
import { counterContext } from 'Context/countContext';

export const App = () => {
    const { count, handleAdd } = useContext(counterContext);

    return (
        <Grid
            header={
                <Between>
                    <Line>
                        <NavLink to='/home'>Home</NavLink>
                        <NavLink to='/about'>About</NavLink>
                        <NavLink to='/lifecycle'>Cycle</NavLink>
                        <NavLink to='/tweets'>Tweets</NavLink>
                        <NavLink to='/refs'>Refs</NavLink>
                        <NavLink to='/hooks'>Hooks</NavLink>
                        <NavLink to='/dropdown'>Dropdown</NavLink>
                    </Line>

                    <Line>
                        <h4>count, {count}</h4>
                        <Icon icon="heart" />
                        <Icon icon="user" />
                        <Icon icon="cog" />
                    </Line>
                </Between>
            }

            section={
                <Rows>
                    <Routes>
                        <Route path='/home/child' element={<h3>child</h3>} />
                        <Route path='/about' element={<AboutView />} />
                        <Route path='/cube' element={<CubeGen />} />
                        <Route path='/*' element={<HomeView />} />
                        <Route path='/lifecycle' element={<LifeCycleView />} />
                        <Route path='/tweets' element={<TweetsView />} />
                        <Route path='/refs' element={<RefsView />} />
                        <Route path='/hooks' element={<HooksView />} />
                        <Route path='/dropdown' element={<DropdownView />} />
                    </Routes>
                </Rows>
            }

            footer={
                <Line>
                    <h4>footer</h4>
                    <h4>footer</h4>
                    <h4>footer</h4>
                    <Btn onClick={handleAdd}>Add</Btn>
                </Line>
            }
        />
    )
}


//<User />