import { createPortal } from "react-dom"
import { Icon } from "../UIKit/Elements/Icon/Icon"
import { Between } from "../UIKit/Layouts/Line/Line"

export const Modal = (props) => {
    const style = {
        position: 'fixed',
        inset: '0',
        padding: 'var(--gap)',
        backgroundColor: '#a1a1a18f'
    }
    return createPortal(
        <div style={style} onClick={props.onClose} >
            <Between>
                <h1>{props.text}</h1>
                <Icon icon="times" />
            </Between>
        </div>,
        document.getElementById('modal')
    )
}